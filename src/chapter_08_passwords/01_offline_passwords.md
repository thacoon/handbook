Easy to remember and secure passwords
=====================================

Remembering complex passwords can be hard but there are a few tips that can help you to remember safe passwords. Password manager are good to manage lot of complex and random passwords but you still need to remember a few passwords like your master password for your password manager or encryption passwords for your hard disks. But even if password manager are developed with security in mind they can fail. If your master password get leaked or in the password manager is a security hole all your passwords can be in danger.

Diceware Passwords
------------------

Diceware is a method for creating passwords. It originally uses a dice and a word list where each word corresponds to a five-digit number. Each digit is a number between 1 and 6. Now you roll the dice five times and then you select the corresponding word. And by generating several words in a sequence you generate your password.

If you do not want to use a diceware word list you can also use a dictionary. Again you need to generate a random number that this time corresponds to a page and then at that page you pick a random word.

Sentences
---------

Another way to create strong and easy to remember passwords is to use sentences.

A few examples:

 * `IloveDouglasAdamsbecausehe'sreallyawesome.`
 * `Peoplelovemachinesin2029A.D.`
 * `BarneyfromHowIMetYourMotherisAWESOME!`

Sentences are easy to remember, even if they are 50 characters long and contain uppercase characters, lowercase characters, symbols and numbers
